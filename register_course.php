<?php 
//Start the session to see if the user is authenticated user. 
session_start(); 
//Check if the user is authenticated first. Or else redirect to login page 
if(isset($_SESSION['IS_AUTHENTICATED']) && $_SESSION['IS_AUTHENTICATED'] == 1){ 
require('menu.php'); 
} 
else{ 
header('location:login_form.php'); 
exit(); 
} 
?> 
<html> 
<body> 
<center> 
<h1>Course Registration/Updation Form</h1> 
<form action="edit_course.php" method="post"> 
<table cellpadding = "10"> 
<tr> 
<td>*COURSE ID</td> 
<td><input type="text" name="cid" maxlength="15"></td> 
</tr> 
<tr>  
<td>COURSE NAME</td> 
<td><input type="text" name="course_name" maxlength="15"></td> 
</tr>
<tr>
<td>INSTRUCTOR</td> 
<td><input type="number_format" name="instructor" maxlength="15"></td> 
</tr> 
<td>DURATION</td> 
<td><input type="text" name="duration" maxlength="15"></td> 
</tr> 
<td>PRICE</td> 
<td><input type="number_format" name="price" maxlength="15"></td> 
</tr> 
<td>CERTIFICATION STATUS</td> 
<td><input type="text" name="cert_stat" maxlength="15"></td> 
</tr> 
<td>DOMAIN ID</td> 
<td><input type="text" name="dom_id" maxlength="15"></td> 
</tr> 
<td>FINANCIAL AID AVAILABITY(available/not available)</td> 
<td><input type="text" name="financial_aid_availability" maxlength="15"></td> 
</tr> 
<td>UNIVERSITY ID</td> 
<td><input type="text" name="u_id" maxlength="15"></td> 
</tr> 
<td>LEVEL</td> 
<td><input type="text" name="level" maxlength="15"></td> 
</tr> 
<td>DESCRIPTION OF COURSE</td> 
<td><input type="text" name="description" maxlength="60"></td> 
</tr> 
<td>PREREQUISITES</td> 
<td><input type="text" name="prerequisites" maxlength="60"></td> 
</tr> 

<td><input type="submit" name="submit" value="Insert"></td> 
<td><input type="submit" name="submit" value="Update (price or certification status or aid availability"></td> 
<td><input type="submit" name="submit" value="Delete"></td> 
</tr> 
</table> 
</form> 
</center> 
</body> 
</html>