<?php
//Start the session to see if the user is authenticated user. 
session_start(); 
//Check if the user is authenticated first. Or else redirect to login page 
if(isset($_SESSION['IS_AUTHENTICATED']) && $_SESSION['IS_AUTHENTICATED'] == 1){ 
require('menu.php'); 
// Code to be executed when 'Insert' is clicked. 
if ($_POST['submit'] == 'Insert'){
//validation flag to check that all validations are done 
$validationFlag = true; 
//Check all the required fields are filled 
if(!($_POST['cid']))
{ 
echo 'All the fields marked as * are compulsary.<br>'; 
$validationFlag = false; 
} 

else{ 
$cid = $_POST['cid']; 
$course_name= $_POST['course_name']; 
$instructor = $_POST['instructor']; 
$duration = $_POST['duration']; 
$price = $_POST['price']; 
$cert_stat = $_POST['cert_stat']; 
$dom_id = $_POST['dom_id']; 
$financial_aid_availibility = $_POST['financial_aid_availibility ']; 
$u_id = $_POST['u_id']; 
$description = $_POST['description']; 
$prerequisites = $_POST['prerequisites']; 
}


//If all validations are correct, connect to MySQL and execute the query 
if($validationFlag){ 
//Connect to mysql server 
$link = mysql_connect('localhost', 'root', ''); 
//Check link to the mysql server 
if(!$link){ 
die('Failed to connect to server: ' . mysql_error()); 
} 
//Select database 
$db = mysql_select_db('courses'); 
if(!$db){ 
die("Unable to select database"); 
} 
//Create Insert query 
$query = "INSERT INTO course (cid,course_name,instructor,duration,price,cert_stat,dom_id,financial_aid_availibility,u_id,description,prerequisites) VALUES ('$cid','$course_name','$instructor','$duration','$price','$cert_stat','$dom_id','$financial_aid_availibility','$u_id','$description','$prerequisites')"; 
//Execute query 
$results = mysql_query($query); 
 
//Check if query executes successfully 
if($results == FALSE) 
echo mysql_error() . '<br>'; 
else 
echo 'Data inserted successfully! '; 
} 
} 
 
// Code to be executed when 'Update' is clicked. 
if ($_POST['submit'] == 'Update'){ 
if(!$_POST['cid']) 
echo 'Enter the customer id as it is the primary key.'; 
else{ 
$validationFlag = true;

$cid = $_POST['cid']; 
$course_name= $_POST['course_name']; 
$instructor = $_POST['instructor']; 
$duration = $_POST['duration']; 
$price = $_POST['price']; 
$cert_stat = $_POST['cert_stat']; 
$dom_id = $_POST['dom_id']; 
$financial_aid_availibility = $_POST['financial_aid_availibility ']; 
$u_id = $_POST['u_id']; 
$description = $_POST['description']; 
$prerequisites = $_POST['prerequisites']; 
 
//$update = "UPDATE customer SET customer_name = '$customer_name'"; 

if($_POST['price']){ 
$update = "UPDATE course SET price = '$price' WHERE cid = '$cid'"; 
} 
if($_POST['cert_stat']){ 
$update = "UPDATE course SET cert_stat = '$cert_stat' WHERE cid = '$cid'"; 
} 
if($_POST['financial_aid_availibility ']){ 
$update = "UPDATE course SET financial_aid_availibility = '$financial_aid_availibility' WHERE cid = '$cid'"; 
} 

//If all validations are correct, connect to MySQL and execute the query 
if($validationFlag){ 
//Connect to mysql server 
$link = mysql_connect('localhost', 'root', ''); 
//Check link to the mysql server 
if(!$link){ 
die('Failed to connect to server: ' . mysql_error()); 
} 
//Select database 
$db = mysql_select_db('courses'); 
if(!$db){ 
die("Unable to select database"); 
} 
//Execute query 
$results = mysql_query($update); 
if($results == FALSE) 
echo mysql_error() . '<br>'; 
else 
echo mysql_info(); 
} 
} 
} 
// Code to be executed when 'Delete' is clicked. 
if ($_POST['submit'] == 'Delete'){ 
if(!$_POST['cid']) 
echo 'Enter the name of the course as it is the primary key.'; 
else{ 

$cid = $_POST['cid']; 
$course_name= $_POST['course_name']; 
$instructor = $_POST['instructor']; 
$duration = $_POST['duration']; 
$price = $_POST['price']; 
$cert_stat = $_POST['cert_stat']; 
$dom_id = $_POST['dom_id']; 
$financial_aid_availibility = $_POST['financial_aid_availibility ']; 
$u_id = $_POST['u_id']; 
$description = $_POST['description']; 
$prerequisites = $_POST['prerequisites']; 

$delete = "DELETE FROM course WHERE cid = '$cid'"; 
//Connect to mysql server 
$link = mysql_connect('localhost', 'root', ''); 
//Check link to the mysql server 
if(!$link){ 
die('Failed to connect to server: ' . mysql_error()); 
} 
//Select database 
$db = mysql_select_db('courses'); 
if(!$db){ 
die("Unable to select database"); 
} 
//Execute query 
$results = mysql_query($delete); 
if($results == FALSE) 
echo mysql_error() . '<br>'; 
else 
echo 'Data deleted successfully'; 
} 
} 
} 
else{ 
header('location:loginform.php'); 
exit(); 
} 
?>
